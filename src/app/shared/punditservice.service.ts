import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PunditserviceService {

// Define API
    // eslint
    //apiURL = 'http://localhost:8000/api/v1/';
    apiURL = 'https://db.snru.ac.th:8000/pundit65';
    apiURLSTD = 'https://db.snru.ac.th:8000/pundit_id';
    constructor(private http: HttpClient) { }
    /*========================================
      CRUD Methods for consuming RESTful API
    =========================================*/
    // Http Options
    // eslint-disable-next-line @typescript-eslint/member-ordering
    httpOptions = {
        headers: new HttpHeaders({
            // eslint-disable-next-line @typescript-eslint/naming-convention
            'Content-Type': 'application/json',
            // eslint-disable-next-line @typescript-eslint/naming-convention
            //'Authorization': 'Bearer ' + localStorage.getItem('accessToken') ?? '',
        }),
    };


    getALL(): Observable<any> {
        return this.http
            .get<any>(this.apiURL, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    getSTDID(id): Observable<any> {
        return this.http
            .get<any>(this.apiURLSTD + '/'+ id, this.httpOptions)
            .pipe(catchError(this.handleError));
    }



    // Error handling
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    handleError(error: any) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(() => errorMessage);
    }
}

import { TestBed } from '@angular/core/testing';

import { PunditserviceService } from './punditservice.service';

describe('PunditserviceService', () => {
  let service: PunditserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PunditserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

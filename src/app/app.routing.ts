import { Route } from '@angular/router';
import { LayoutComponent } from 'app/layout/layout.component';


// @formatter:off
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    {path: '', pathMatch : 'full', redirectTo: 'view'},


    {
        path: '',
        //canActivate: [NoAuthGuard],
        //canActivateChild: [NoAuthGuard],
        component  : LayoutComponent,
        data: {
            layout: 'modern'
        },
        children   : [
            {path: 'view', loadChildren: () => import('app/modules/pundit/pundit.module').then(m => m.PunditModule)},
        ]
    },

    {
        path: '**', redirectTo: 'view'
      },


];

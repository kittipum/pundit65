import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PunditDetailComponent } from './pundit-detail.component';

describe('PunditDetailComponent', () => {
  let component: PunditDetailComponent;
  let fixture: ComponentFixture<PunditDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PunditDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PunditDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

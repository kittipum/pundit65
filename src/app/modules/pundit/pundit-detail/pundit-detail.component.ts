import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ParamMap, Router, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, take } from 'rxjs/operators';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { fuseAnimations } from '@fuse/animations';
import { PunditserviceService } from 'app/shared/punditservice.service';
import { thru } from 'lodash';

@Component({
  selector: 'app-pundit-detail',
  templateUrl: './pundit-detail.component.html',
  styleUrls: ['./pundit-detail.component.scss']
})
export class PunditDetailComponent implements OnInit, OnDestroy {
    idstd: any;
    isLoading: boolean = true;
    datastd: any = [];
    sub: any = [];
    numrow: string;
    numreal: string;
    build: string;
    room: string;
    constructor(
        private _apiall: PunditserviceService,
        private _router: Router,
        private _activatedRoute: ActivatedRoute) {
            this.idstd = this._activatedRoute.snapshot.params.id;
    }


  ngOnInit(): void {
    this.isLoading = true;
    this.getstd();
  }

  async getstd(): Promise<void> {
    this.isLoading = true;
    console.log(this.idstd);

    const subx1 = await this._apiall.getSTDID(this.idstd).pipe(take(1), map((data) => {
        console.log(data);
        this.datastd = data[0];

        this.numrow = this.datastd.NUMROW;
        this.numreal = this.datastd.NUMREAL;
        this.build = this.datastd.BUILDNAME;
        this.room = this.datastd.ROOM_SUB;
        //this.setPage(1);
        //console.log(this.examlist);
    }))
        .subscribe({
            error: e => console.error(e),
            // eslint-disable-next-line no-console
            complete: () => {
                this.isLoading = false;
                // eslint-disable-next-line no-console
                console.info('complete');
            }
        });

    this.sub.push(subx1);

    }

    ngOnDestroy(): void {
        if (this.sub != null) {
            this.sub.forEach(sub => sub.unsubscribe());
        }
      }

}

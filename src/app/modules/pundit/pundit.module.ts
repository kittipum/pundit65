import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { CdkScrollableModule } from '@angular/cdk/scrolling';
import { FuseCardModule } from '@fuse/components/card';
import { FuseLoadingBarModule } from '@fuse/components/loading-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FuseScrollbarModule } from '@fuse/directives/scrollbar';
import { FuseConfirmationModule } from '@fuse/services/confirmation';
import { FuseAlertModule } from '@fuse/components/alert';
import { NgxPaginationModule } from 'ngx-pagination';

import { PunditHomeComponent } from './pundit-home/pundit-home.component';
import { PunditDetailComponent } from './pundit-detail/pundit-detail.component';
import { punditRoutes } from './pundit.routing';


@NgModule({
  declarations: [
    PunditHomeComponent,
    PunditDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(punditRoutes),
    FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatMenuModule,
        MatRadioModule,
        FuseCardModule,
        FuseLoadingBarModule,
        MatProgressBarModule,
        FuseScrollbarModule,
        FuseConfirmationModule,
        CdkScrollableModule,
        FuseAlertModule,
        NgxPaginationModule

  ]
})
export class PunditModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PunditHomeComponent } from './pundit-home.component';

describe('PunditHomeComponent', () => {
  let component: PunditHomeComponent;
  let fixture: ComponentFixture<PunditHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PunditHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PunditHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

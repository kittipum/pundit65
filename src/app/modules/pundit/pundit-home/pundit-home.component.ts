/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ParamMap, Router, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, take } from 'rxjs/operators';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { fuseAnimations } from '@fuse/animations';
import { PunditserviceService } from 'app/shared/punditservice.service';
import { thru } from 'lodash';

//import { PagerService } from 'app/shared/pageservice';



@Component({
  selector: 'app-pundit-home',
  templateUrl: './pundit-home.component.html',
  styleUrls: ['./pundit-home.component.scss']
})
export class PunditHomeComponent implements OnInit, OnDestroy {

    POSTS: any;
  page: number = 1;
  count: number = 0;
  tableSize: number = 7;
  tableSizes: any = [20, 50, 100];

num =0;

    subscriptions = [];
    mytxt: string;
    searchText: string;
    private sub: any;
    isLoading = false;
    title = 'Simple Datatable Example using Angular 4';
    public data: Object;
    public temp_var: Object = false;
    constructor(private punditservice: PunditserviceService,  ) { }

    // array of all items to be paged
    allkeep: any;
    private allItems: any;
    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];

    ngOnInit(): void {

      this.getall();
    }



    onkey(findx: any): void {
      //console.log(findx);
      if (findx.target.value !== '' && findx.target.value !== null) {
        this.mytxt = findx.target.value;
        //console.log(this.mytxt);
        /*this.sub = this.http.get('http://192.168.201.165:8001/api/find/' + findx.target.value).subscribe((res: Response) => {
          this.allItems = res;
          this.setPage(1);
          console.log(this.allItems);
          });*/
          this.allItems = this.allkeep;
          this.allItems = this.allItems.filter(s => s.STUDENT_ID.includes(this.mytxt) || s.FULL_NAME_STD.includes(this.mytxt) );
          //this.setPage(1);
          this.POSTS = this.allItems;
          this.page = 1;
      } else {
        //console.log('getall');
        this.getallx();
      }
    }



    async getall(): Promise<void> {
        this.isLoading = true;

        const subx1 = await this.punditservice.getALL().pipe(take(1), map((data) => {
            this.num = 0;
            data.forEach((o) => {
                this.num = this.num+1;
                o.numx = this.num;
            });
            this.POSTS = data;
            this.allkeep = data;
            //console.log(data);
            this.allItems = this.allkeep;
            this.page = 1;
            //this.setPage(1);
            //console.log(this.examlist);
        }))
            .subscribe({
                error: e => console.error(e),
                // eslint-disable-next-line no-console
                complete: () => {
                    this.isLoading = false;
                    // eslint-disable-next-line no-console
                    console.info('complete');
                }
            });

        this.subscriptions.push(subx1);

    }

     onTableDataChange(event: any): void {
        this.page = event;
        //this.getall();
      }
      onTableSizeChange(event: any): void {
        this.tableSize = event.target.value;
        this.page = 1;
        //this.getall();
      }



    getallx(): void {
        //console.log('keep');
        //console.log(this.allkeep);
        this.allItems = this.allkeep;
        this.POSTS = this.allItems;
        this.page = 1;
        //this.setPage(1);
    }

    ngOnDestroy(): void {
      if (this.sub != null) {
        this.sub.unsubscribe();
      }
    }

  }

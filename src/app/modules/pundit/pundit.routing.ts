import { Route } from '@angular/router';
import { PunditHomeComponent } from './pundit-home/pundit-home.component';
import { PunditDetailComponent } from './pundit-detail/pundit-detail.component';
export const punditRoutes: Route[] = [
    {
        path     : '',
        component: PunditHomeComponent
    },
    {
        path     : ':id',
        component: PunditDetailComponent
    }
];
